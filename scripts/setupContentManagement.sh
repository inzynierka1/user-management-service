#!/usr/bin/env bash

docker run -d -p 27018:27017 mongo
docker kill content-management
docker rm content-management
docker run -d --name=content-management -p 8081:8081 \
    -e ME_ENGINEER_MONGO_CONTENT=docker.for.mac.localhost:27018 \
    -e ME_ENGINEER_IMAGES_STORAGE_PATH=/usr/storage \
    bartekf95/content-management-service