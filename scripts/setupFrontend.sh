#!/usr/bin/env bash

docker kill frontend
docker rm frontend
docker run -d --name=frontend -p 8000:8000 bartekf95/frontend-service
