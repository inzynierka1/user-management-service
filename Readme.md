#How to run?
* `./gradlew buildDocker (or intellij task)`
* `docker run -d -p 27017:27017 mongo`
* `docker run -d -p 8080:8080 me.engineer.user-management-service`

##To enable run from intellij:
#####Add to /etc/hosts
`127.0.0.1       docker.for.mac.localhost`