package me.enginner.service;

import me.enginner.domain.User;

public interface DiscoverService {
    User getLoggedUser();
    String getLoggedUserId();
}