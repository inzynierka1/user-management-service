package me.enginner.service;

import me.enginner.domain.Group;
import me.enginner.domain.UserInGroup;
import me.enginner.repository.GroupRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class GroupServiceImpl implements GroupService {

    private final GroupRepo groupRepo;

    @Autowired
    public GroupServiceImpl(GroupRepo groupRepo) {
        this.groupRepo = groupRepo;
    }

    public String getUserGroupIds(String userId) {
//        List<String> groupIds = groupRepo.findAllByUsersContains(userId)
//                .stream()
//                .map(Group::getStringId)
//                .collect(Collectors.toList());

        List<String> groupIds = groupRepo.findAll().stream()
                .filter(group -> group.getUsers().stream().map(UserInGroup::getUserId).anyMatch(userId::equals))
                .map(Group::getStringId)
                .collect(Collectors.toList());

        return concatGroupIdsIntoQuery(groupIds);
    }

    private String concatGroupIdsIntoQuery(List<String> groupIds) {
        if (groupIds.isEmpty())
            return "";
        StringBuilder groupsQuery = new StringBuilder();
        for(String groupId : groupIds){
            groupsQuery.append(groupId).append(",");
        }
        return groupsQuery.deleteCharAt(groupsQuery.length() - 1).toString();
    }
}
