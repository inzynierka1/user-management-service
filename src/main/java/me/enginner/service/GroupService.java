package me.enginner.service;

public interface GroupService {
    String getUserGroupIds(String userId);
}
