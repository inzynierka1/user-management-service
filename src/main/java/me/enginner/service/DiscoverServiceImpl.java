package me.enginner.service;

import me.enginner.api.exception.UnauthorizedException;
import me.enginner.domain.User;
import me.enginner.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DiscoverServiceImpl implements DiscoverService {

    private final UserRepo userRepo;

    @Autowired
    public DiscoverServiceImpl(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @Override
    public User getLoggedUser() {
        return userRepo.findOne(this.getLoggedUserId());
    }

    @Override
    public String getLoggedUserId() {
        return getAuthUser()
                .orElseThrow(() -> new UnauthorizedException("You need to authorize to perform this operation"))
                .getName();
    }

    private Optional<Authentication> getAuthUser() {
         return Optional.ofNullable(SecurityContextHolder.getContext().getAuthentication());
    }
}
