package me.enginner.domain;

import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;

@Data
public class User {

    @Id
    private ObjectId id;
    private String email;
    private String password;
    private LocalDateTime since;

    private User(String email, String password){
        this.id = ObjectId.get();
        this.email = email;
        this.password = password;
        this.since = LocalDateTime.now();
    }

    public static User register(String email, String password){
        return new User(email, password);
    }

    public String getStringId() {
        return id.toString();
    }
}
