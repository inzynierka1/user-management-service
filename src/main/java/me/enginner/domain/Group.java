package me.enginner.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
public class Group {

    @Id
    private ObjectId id;
    private String name;
    private String owner;
    private List<UserInGroup> users;
    private LocalDateTime creation;

    private Group(String name, String owner) {
        this.id = ObjectId.get();
        this.name = name;
        this.owner = owner;
        this.users = new ArrayList<>();
        this.creation = LocalDateTime.now();
    }

    private Group(String name, String owner, List<User> users) {
        this(name, owner);
        addUsers(users);
    }

    private void addUser(User user) {
        if(!alreadyInList(user.getStringId())){
            users.add(new UserInGroup(user.getStringId(), user.getEmail()));
        }
    }

    public void addUsers(List<User> users){
        users.forEach(this::addUser);
    }

    public String getStringId(){
        return id.toString();
    }

    private boolean alreadyInList(String userId) {
        return this.users.stream()
                .anyMatch(user -> user.isIdEqual(userId));
    }

    public static Group createNew(String name, String owner, List<User> users) {
        return new Group(name, owner, users);
    }

    public void deleteUser(String userId) {
        users.removeIf(user -> user.isIdEqual(userId));
    }

    public void update(String name, List<User> users) {
        if (shouldChangeName(name)) {
            this.name = name;
        }
        updateUsers(users);
    }

    private void updateUsers(List<User> updatedUsers) {
        removeUsers(updatedUsers);
        addUsers(updatedUsers);
    }

    private void removeUsers(List<User> updatedUsers) {
        List<UserInGroup> toRemove = users.stream()
                .filter(inGroup -> updatedUsers.stream()
                        .noneMatch(updated -> updated.getStringId().equals(inGroup.getUserId())))
                .collect(Collectors.toList());

        users.removeAll(toRemove);
    }

    private boolean shouldChangeName(String param){
        return param != null
                && !"".equals(param)
                && !this.name.equals(param);
    }
}
