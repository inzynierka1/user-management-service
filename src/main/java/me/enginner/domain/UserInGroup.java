package me.enginner.domain;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class UserInGroup {
    private String userId;
    private String email;
    private LocalDateTime added;

    UserInGroup(String userId, String email) {
        this.userId = userId;
        this.email = email;
        this.added = LocalDateTime.now();
    }

    boolean isIdEqual(String id){
        return userId.equals(id);
    }
}
