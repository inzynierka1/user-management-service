package me.enginner.config;


import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories("me.enginner.repository")
public class MongoConfig extends AbstractMongoConfiguration {

    @Autowired
    private API api;

    @Override
    protected String getDatabaseName() {
        return api.getDbName() ;
    }

    @Override
    public Mongo mongo() throws Exception {
        return new MongoClient(api.getMongoAddress());
    }
}