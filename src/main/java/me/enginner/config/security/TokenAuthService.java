package me.enginner.config.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.security.crypto.codec.Base64;

import javax.servlet.http.HttpServletRequest;

public class TokenAuthService {

    private final byte[] secret;

    public TokenAuthService(String secret) {
        this.secret = secret.getBytes();
    }

    UserAuthentication getAuthentication(HttpServletRequest httpRequest) {
        String token = httpRequest.getHeader("X-AUTH-TOKEN");
        if (token != null) {
            AuthUser user = parseUserFromToken(token);
            return new UserAuthentication(user);
        }
        return null;
    }

    private AuthUser parseUserFromToken(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(Base64.encode(secret))
                .parseClaimsJws(token)
                .getBody();

        String id = claims.get("id", String.class);
        return new AuthUser(id);
    }
}
