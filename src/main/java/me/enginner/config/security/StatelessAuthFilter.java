package me.enginner.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Component
public class StatelessAuthFilter extends GenericFilterBean {

    //If we want to call default constructor we need to have autowired like this
    @Autowired
    private TokenAuthService tokenAuthService;

    @Override
    public void doFilter(ServletRequest servletRequest,
                         ServletResponse servletResponse,
                         FilterChain filterChain)
            throws IOException, ServletException {
        UserAuthentication authentication = getAuthenticationFromRequest(servletRequest);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        filterChain.doFilter(servletRequest, servletResponse);
        SecurityContextHolder.getContext().setAuthentication(null);
    }

    private UserAuthentication getAuthenticationFromRequest(ServletRequest req) {
        HttpServletRequest http = (HttpServletRequest) req;
        return tokenAuthService.getAuthentication(http);
    }
}