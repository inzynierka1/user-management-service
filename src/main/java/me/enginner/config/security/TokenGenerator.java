package me.enginner.config.security;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import me.enginner.config.SpringSecurityConfig;
import me.enginner.domain.User;
import org.springframework.security.crypto.codec.Base64;

import java.util.HashMap;
import java.util.Map;

public class TokenGenerator {

    private static final String JWT = "JWT";
    private static final String TYP_KEY = "typ";

    private TokenGenerator(){}

    public static String generateToken(User user) {
        Map<String,Object> claims = new HashMap<>();
        claims.put("id", user.getStringId());
        return Jwts.builder()
                .setClaims(claims)
                .setHeaderParam(TYP_KEY, JWT)
                .signWith(SignatureAlgorithm.HS256, Base64.encode(SpringSecurityConfig.SECRET.getBytes()))
                .compact();
    }
}
