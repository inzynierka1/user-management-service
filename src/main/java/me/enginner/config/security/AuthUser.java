package me.enginner.config.security;

public class AuthUser {

    private String id;

    public AuthUser(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
