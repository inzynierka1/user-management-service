package me.enginner.config;

import lombok.*;

@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class API {
    @Getter @NonNull
    private String mongoAddress;
    @Getter @NonNull
    private String dbName;
    @Getter @NonNull
    private String contentEndpoint;
}
