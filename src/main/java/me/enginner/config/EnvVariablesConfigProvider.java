package me.enginner.config;

import lombok.extern.slf4j.Slf4j;

import java.util.Map;
import java.util.Optional;

/**
 * Created by Bartosz Frankowski on 18.10.2017
 */
@Slf4j
public class EnvVariablesConfigProvider implements ConfigProvider {
    private static final String ENV_MONGO_AUTH_MANAGEMENT = "ME_ENGINEER_MONGO_AUTH";
    private static final String DEFAULT_MONGO_ADDRESS = "localhost:32768";
    private static final String ENV_MONGO_DB_NAME_AUTH = "ME_ENGINEER_MONGO_DB_NAME_AUTH";
    private static final String DEFAULT_MONGO_DB_NAME = "NetworkDB";
    private static final String ENV_CONTENT_MANAGEMENT_ADDRESS_AUTH_MANAGEMENT = "ME_ENGINEER_CONTENT_MANAGEMENT_ADDRESS_AUTH";
    private static final String DEFAULT_CONTENT_MANAGEMENT_ADDRESS = "http://localhost:8081/api";

    @Override
    public API getApiConfig() {
        Map<String, String> environmentalVariables = System.getenv();
        String mongoAddress = getMongoAddress(environmentalVariables).orElse(DEFAULT_MONGO_ADDRESS);
        String dbName = getDbName(environmentalVariables).orElse(DEFAULT_MONGO_DB_NAME);
        String contentManagementAddress = getContentManagementAdress(environmentalVariables).orElse(DEFAULT_CONTENT_MANAGEMENT_ADDRESS);
        return API.builder()
                .mongoAddress(mongoAddress)
                .dbName(dbName)
                .contentEndpoint(contentManagementAddress)
                .build();
    }

    private Optional<String> getMongoAddress(Map<String, String> environmentalVariables) {
        if(environmentalVariables.containsKey(ENV_MONGO_AUTH_MANAGEMENT)) {
            String mongoAddress = environmentalVariables.get(ENV_MONGO_AUTH_MANAGEMENT);
            log.info("MongoDB address obtained: {}", mongoAddress);
            return Optional.of(mongoAddress);
        }
        log.warn("Env {} not found! Using default MongoDB address: {}", ENV_MONGO_AUTH_MANAGEMENT, DEFAULT_MONGO_ADDRESS);
        return Optional.empty();
    }

    private Optional<String> getDbName(Map<String, String> environmentalVariables) {
        if(environmentalVariables.containsKey(ENV_MONGO_DB_NAME_AUTH)) {
            String dbName = environmentalVariables.get(ENV_MONGO_DB_NAME_AUTH);
            log.info("DB name found: {}", dbName);
            return Optional.of(dbName);
        }
        log.warn("Env {} not found! Using default DB name: {}", ENV_MONGO_DB_NAME_AUTH, DEFAULT_MONGO_DB_NAME);
        return Optional.empty();
    }

    private Optional<String> getContentManagementAdress(Map<String, String> environmentalVariables) {
        if(environmentalVariables.containsKey(ENV_CONTENT_MANAGEMENT_ADDRESS_AUTH_MANAGEMENT)) {
            String contentManagementAddress = environmentalVariables.get(ENV_CONTENT_MANAGEMENT_ADDRESS_AUTH_MANAGEMENT);
            log.info("ContentManagement address obtained: {}", contentManagementAddress);
            return Optional.of(contentManagementAddress);
        }
        log.warn("Env {} not found! Using default ContentManagement address: {}", 
                ENV_CONTENT_MANAGEMENT_ADDRESS_AUTH_MANAGEMENT, DEFAULT_CONTENT_MANAGEMENT_ADDRESS);
        return Optional.empty();
    }
}
