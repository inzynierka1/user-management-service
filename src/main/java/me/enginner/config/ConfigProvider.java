package me.enginner.config;

/**
 * Created by Bartosz Frankowski on 18.10.2017
 */
public interface ConfigProvider {
    API getApiConfig();
}
