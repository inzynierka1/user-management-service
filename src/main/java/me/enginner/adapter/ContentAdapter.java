package me.enginner.adapter;

import me.enginner.adapter.dto.Search;
import me.enginner.adapter.events.ContentEvent;

import java.util.List;

public interface ContentAdapter {

    List<String> getUsersEvents(String userId, String groupIds);
    List getSelfEvents(String userId);
    String getEventDetails(String eventId);

    String sendEvent(ContentEvent contentEvent);

    boolean deleteEvent(String eventId);

    boolean updateEvent(String eventId, me.enginner.api.event.EventRequest request);

    String searchEvents(Search search);
}