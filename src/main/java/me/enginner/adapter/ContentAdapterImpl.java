package me.enginner.adapter;

import lombok.extern.slf4j.Slf4j;
import me.enginner.adapter.dto.Search;
import me.enginner.adapter.events.ContentEvent;
import me.enginner.config.API;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;

@Slf4j
public class ContentAdapterImpl implements ContentAdapter {
    private API api;

    public ContentAdapterImpl(API api) {
        this.api = api;
    }

    @Override
    public String getEventDetails(String eventId) {
        String url = api.getContentEndpoint() + "/events/" + eventId;
        try {
            return new RestTemplate().getForObject(url, String.class);
        } catch (Exception e){
            errorLog("get", url);
            return "";
        }
    }

    @Override
    public List getUsersEvents(String userId, String groupIds) {
        String url = api.getContentEndpoint() + "/events/user/" + userId + "/" + groupIds;
        try {
            return new RestTemplate().getForObject(url, List.class);
        } catch (Exception e) {
            errorLog("get", url);
            return Collections.emptyList();
        }
    }

    @Override
    public List getSelfEvents(String userId) {
        String url = api.getContentEndpoint() + "/events/user/" + userId;
        try {
            return new RestTemplate().getForObject(url, List.class);
        } catch (Exception e) {
            errorLog("get", url);
            return Collections.emptyList();
        }
    }

    @Override
    public String sendEvent(ContentEvent contentEvent) {
        String url = api.getContentEndpoint() + "/events";
        try {
            return new RestTemplate().postForObject(url, contentEvent, String.class);
        } catch (Exception e) {
            errorLog("post", url);
            return "";
        }
    }

    @Override
    public boolean deleteEvent(String eventId) {
        String url = api.getContentEndpoint() + "/events/" + eventId;
        try {
            new RestTemplate().delete(url);
            return true;
        } catch (Exception e) {
            errorLog("delete", url);
            return false;
        }
    }

    @Override
    public boolean updateEvent(String eventId, me.enginner.api.event.EventRequest request) {
        String url = api.getContentEndpoint() + "/events/" + eventId;
        try {
            new RestTemplate().put(url, request);
            return true;
        } catch (Exception e) {
            errorLog("put", url);
            return false;
        }
    }

    @Override
    public String searchEvents(Search query) {
        String url = api.getContentEndpoint() + "/events" + query.generateSearchQuery();
        try {
            return new RestTemplate().getForObject(url, String.class);
        } catch (Exception e) {
            errorLog("get", url);
            return "";
        }
    }

    private void errorLog(String method, String url) {
        log.error("Error with " + method + " on: " + url);
    }
}
