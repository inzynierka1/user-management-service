package me.enginner.adapter.events;

import lombok.Data;

import java.util.List;

@Data
public class ContentEvent {

    private String userId;
    private String title;
    private String content;
    private List<String> groups;

    private ContentEvent(String userId,
                         String title,
                         String content,
                         List<String> groups){
        this.userId = userId;
        this.title = title;
        this.content = content;
        this.groups = groups;
    }

    public static ContentEvent create(me.enginner.api.event.EventRequest request, String userId) {
        return new ContentEvent(userId, request.getTitle(), request.getContent(), request.getGroups());
    }
}
