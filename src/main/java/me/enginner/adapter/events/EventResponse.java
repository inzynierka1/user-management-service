package me.enginner.adapter.events;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class EventResponse {

    private String id;
    private String userId;
    private String timestamp;
    private String title;
    private String content;

    @Override
    public String toString() {
        return "EventResponse{" +
                "id='" + id + '\'' +
                ", userId='" + userId + '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
