package me.enginner.adapter.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Search {

    private String titleQuery;
    private String contentQuery;
    private String groupId;

    public String generateSearchQuery(){
        List<String> requestParams = generateRequestParams();
        return constructQuery(requestParams);
    }

    private List<String> generateRequestParams(){
        List<String> params = new ArrayList<>();
        if (hasTitleQuery()){
            params.add("title=" + this.titleQuery);
        }
        if (hasContentQuery()){
            params.add("content=" + this.contentQuery);
        }
        if (hasGroupId()){
            params.add("groupId=" + this.groupId);
        }
        return params;
    }

    private boolean hasTitleQuery(){
        return isNotNullAndNotEmpty(titleQuery);
    }

    private boolean hasContentQuery(){
        return isNotNullAndNotEmpty(contentQuery);
    }

    private boolean hasGroupId(){
        return isNotNullAndNotEmpty(groupId);
    }

    private String constructQuery(List<String> requestParams) {
        if(requestParams.size() == 0){
            return "";
        }
        StringBuilder response = new StringBuilder().append("?");
        for (String param : requestParams){
            response.append(param).append("&");
        }
        return response.deleteCharAt(response.length() - 1).toString();
    }

    private boolean isNotNullAndNotEmpty(String param){
        return param != null && !"".equals(param);
    }
}
