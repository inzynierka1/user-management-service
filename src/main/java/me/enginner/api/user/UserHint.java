package me.enginner.api.user;

import lombok.Data;

@Data
class UserHint {
    private String userId;
    private String email;

    UserHint(String userId, String email) {
        this.userId = userId;
        this.email = email;
    }
}
