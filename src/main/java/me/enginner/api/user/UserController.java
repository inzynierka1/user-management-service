package me.enginner.api.user;

import me.enginner.api.user.payload.UserDetailsResponse;
import me.enginner.repository.UserRepo;
import me.enginner.service.DiscoverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class UserController {

    private final UserRepo userRepo;
    private final DiscoverService discoverService;
    private final UserMapper mapper;

    @Autowired
    public UserController(UserRepo userRepo,
                          DiscoverService discoverService) {
        this.userRepo = userRepo;
        this.discoverService = discoverService;
        this.mapper = new UserMapper();
    }

    @GetMapping(value = "/api/self")
    public UserDetailsResponse getDetails(){
        return mapper.map(discoverService.getLoggedUser());
    }

    @GetMapping(value = "/api/users/search")
    public List<UserDetailsResponse> searchUser(@RequestParam(value = "email", required = false) String requestEmail){
        // todo some trivial search algorithm ??
        return userRepo.findTop10ByEmailContaining(requestEmail)
                .stream()
                .map(mapper::map)
                .collect(Collectors.toList());
    }

    @GetMapping(value = "/api/users/names")
    public List<UserHint> getAllUserNames() {
        String loggedUserId = discoverService.getLoggedUserId();
        return userRepo.findAll()
                .stream()
                .filter(user -> !user.getStringId().equals(loggedUserId))
                .map(user -> new UserHint(user.getStringId(), user.getEmail()))
                .collect(Collectors.toList());
    }
}
