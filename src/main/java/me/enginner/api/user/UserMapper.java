package me.enginner.api.user;

import me.enginner.api.user.payload.UserDetailsResponse;
import me.enginner.domain.User;

class UserMapper {

    UserMapper() {}

    UserDetailsResponse map(User user){
        if(user == null) {
            return null;
        }
        return new UserDetailsResponse(user.getStringId(), user.getEmail(), user.getSince());
    }
}
