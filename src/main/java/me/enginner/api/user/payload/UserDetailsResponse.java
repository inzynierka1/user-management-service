package me.enginner.api.user.payload;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class UserDetailsResponse {

    private String id;
    private String email;
    private String date;

    public UserDetailsResponse(String stringId,
                               String email,
                               LocalDateTime since) {
        this.id = stringId;
        this.email = email;
        this.date = since.toString();
    }
}