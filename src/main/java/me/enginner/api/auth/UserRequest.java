package me.enginner.api.auth;

class UserRequest {

    private String email;
    private String password;

    public UserRequest() {}

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    boolean isValid(){
        return isNotEmpty() && hasProperLength();
    }

    private boolean isNotEmpty(){
        return this.email != null && this.password != null;
    }

    private boolean hasProperLength(){
        return this.email.length() > 5 && this.password.length() > 5;
    }
}
