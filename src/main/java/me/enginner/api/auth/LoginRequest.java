package me.enginner.api.auth;

class LoginRequest {
    private String login = "def";
    private String password = "def";

    LoginRequest(String[] params){
        this.login = params[0];
        this.password = params[1];
    }

    String getLogin() {
        return login;
    }

    String getPassword() {
        return password;
    }
}
