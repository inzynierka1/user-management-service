package me.enginner.api.auth;

class TokenResponse {

    private String token;

    TokenResponse(String token){
        this.token = token;
    }

    public String getToken() {
        return token;
    }
}
