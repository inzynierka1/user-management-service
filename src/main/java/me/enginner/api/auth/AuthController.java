package me.enginner.api.auth;

import me.enginner.api.exception.AuthorizationException;
import me.enginner.api.exception.BadRequestException;
import me.enginner.config.security.TokenGenerator;
import me.enginner.domain.User;
import me.enginner.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.*;

@RestController
public class AuthController {

    private final UserRepo userRepository;

    @Autowired
    public AuthController(UserRepo userRepository) {
        this.userRepository = userRepository;
    }

    @PostMapping(value = "/api/register")
    public TokenResponse register(@RequestBody UserRequest request) {
        if (request.isValid()) {
            User user = userRepository.findByEmail(request.getEmail());
            if (user != null) {
                throw new BadRequestException("Email already exists.");
            }

            User registeredUser = userRepository.save(User.register(request.getEmail(), request.getPassword()));
            return new TokenResponse(TokenGenerator.generateToken(registeredUser));
        }
        throw new BadRequestException("Wrong credentials. Login and password need to have more than 5 chars.");
    }

    @GetMapping(value = "/api/login")
    public TokenResponse login(@RequestHeader(value = "Authorization") String credentials) {
        LoginRequest cred = decode(credentials);
        User user = userRepository.findByEmail(cred.getLogin());

        if (!areCredentialsValid(user, cred)) {
            throw new AuthorizationException("Authentication failed");
        }

        return new TokenResponse(TokenGenerator.generateToken(user));
    }

    private LoginRequest decode(String credentials){
        return new LoginRequest(new String(Base64Utils.decode(credentials.getBytes())).split(":"));
    }

    private boolean areCredentialsValid(User user, LoginRequest cred) {
        return user != null
                && user.getPassword().equals(cred.getPassword());
    }
}
