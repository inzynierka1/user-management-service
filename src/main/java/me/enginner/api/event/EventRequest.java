package me.enginner.api.event;

import lombok.Data;
import lombok.NoArgsConstructor;
import me.enginner.api.event.payload.ImageResponse;

import java.util.List;

@Data
@NoArgsConstructor
public class EventRequest {

    private String userId;
    private String title;
    private String content;
    private List<ImageResponse> images;
    private List<String> groups;

}
