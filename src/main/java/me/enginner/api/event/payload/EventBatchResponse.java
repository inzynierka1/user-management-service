package me.enginner.api.event.payload;

import java.time.LocalDateTime;
import java.util.List;

public class EventBatchResponse {

    private List<EventResponse> events;
    private LocalDateTime since;
    private LocalDateTime to;

    public EventBatchResponse() {}

    public List<EventResponse> getEvents() {
        return events;
    }

    public LocalDateTime getSince() {
        return since;
    }

    public LocalDateTime getTo() {
        return to;
    }
}
