package me.enginner.api.event.payload;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Bartosz Frankowski on 08.11.2017
 */
@Data
@NoArgsConstructor
public class ImageResponse {

    private String id;
    private String name;
}
