package me.enginner.api.event.payload;

import java.time.LocalDateTime;

public class EventResponse {

    private String id;
    private String title;
    private String content;
    private LocalDateTime created;

    public EventResponse() {}

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public LocalDateTime getCreated() {
        return created;
    }
}
