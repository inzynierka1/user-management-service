package me.enginner.api.event;

import me.enginner.adapter.ContentAdapter;
import me.enginner.adapter.ContentAdapterImpl;
import me.enginner.adapter.dto.Search;
import me.enginner.adapter.events.ContentEvent;
import me.enginner.api.group.GroupResponse;
import me.enginner.config.API;
import me.enginner.domain.Group;
import me.enginner.domain.User;
import me.enginner.repository.GroupRepo;
import me.enginner.service.DiscoverService;
import me.enginner.service.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
public class EventController {

    private final ContentAdapter adapter;
    private final DiscoverService discover;
    private final GroupService groupService;
    private final GroupRepo groupRepo;
    private final JacksonJsonParser parser;

    @Autowired
    public EventController(DiscoverService discover,
                           GroupService groupService,
                           GroupRepo groupRepo,
                           API api) {
        this.adapter = new ContentAdapterImpl(api);
        this.discover = discover;
        this.groupService = groupService;
        this.groupRepo = groupRepo;
        this.parser = new JacksonJsonParser();
    }

    @GetMapping(value = "/api/events")
    public List getAllUserEvents(){
        String userId = discover.getLoggedUserId();
        String groupIds = groupService.getUserGroupIds(userId);
        return adapter.getUsersEvents(userId, groupIds);
    }

    @GetMapping(value = "/api/events/self")
    public List getSelfEvents() {
        String userId = discover.getLoggedUserId();
        return adapter.getSelfEvents(userId);
    }

    @PostMapping(value = "/api/events")
    public String addEvent(@RequestBody EventRequest request) {
        //todo better response code
        return adapter.sendEvent(ContentEvent.create(request, discover.getLoggedUserId()));
    }

    @GetMapping(value = "/api/events/{id}")
    public Map<String, Object> getEvent(@PathVariable String id) {
        String json = adapter.getEventDetails(id);
        Map<String, Object> parsed = parser.parseMap(json);

        List groupsResponse = getGroupResponseForEvent((List) parsed.get("groups"));
        parsed.put("groups", groupsResponse);

        return parsed;
    }

    @PutMapping(value = "/api/events/{id}")
    public void updateEvent(@PathVariable String id,
                            @RequestBody me.enginner.api.event.EventRequest request) {
        adapter.updateEvent(id, request);
    }

    @DeleteMapping(value = "/api/events/{id}")
    public void deleteEvent(@PathVariable String id){
        adapter.deleteEvent(id);
    }

    @GetMapping(value = "/api/events/search")
    public String searchQuery(@RequestParam(value = "title", required = false) String titleQuery,
                              @RequestParam(value = "content", required = false) String contentQuery,
                              @RequestParam(value = "groupName", required = false) String groupNameQuery) {
        String groupId = getGroupIdBasedOnQuery(groupNameQuery);
        return adapter.searchEvents(new Search(titleQuery, contentQuery, groupId));
    }

    private String getGroupIdBasedOnQuery(String query) {
        if(query == null) return "";
        Group group = groupRepo.findFirstByOwnerAndNameContaining(discover.getLoggedUserId(), query);
        if(group == null) return "";
        return group.getStringId();
    }

    private List<GroupResponse> getGroupResponseForEvent(List<String> groupIds) {
        User user = discover.getLoggedUser();
        List<Group> userGroups = groupRepo.findByOwner(user.getStringId());
        List<GroupResponse> groups = new ArrayList<>();

        groupIds.forEach(groupId ->
            userGroups.stream()
                    .filter(group -> group.getStringId().equals(groupId))
                    .findAny()
                    .ifPresent(group -> groups.add(new GroupResponse(group, user)))
        );

        return groups;
    }

}
