package me.enginner.api.group;

import lombok.Data;
import me.enginner.domain.User;
import me.enginner.domain.UserInGroup;

@Data
class UserGroupResponse {
    private String userId;
    private String email;
    private String added;

    UserGroupResponse(User user) {
        this.userId = user.getStringId();
        this.email = user.getEmail();
    }

    UserGroupResponse(UserInGroup userInGroup) {
        this.userId = userInGroup.getUserId();
        this.email = userInGroup.getEmail();
        this.added = userInGroup.getAdded().toString();
    }
}