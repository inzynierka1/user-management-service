package me.enginner.api.group;

import lombok.Data;

import java.util.List;

@Data
class GroupRequest {
    private String name;
    private List<String> users;
}
