package me.enginner.api.group;

import me.enginner.domain.Group;
import me.enginner.domain.User;
import me.enginner.repository.GroupRepo;
import me.enginner.repository.UserRepo;
import me.enginner.service.DiscoverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class GroupController {

    private final DiscoverService discover;
    private final GroupRepo groupRepo;
    private final UserRepo userRepo;

    @Autowired
    public GroupController(DiscoverService discover,
                           GroupRepo groupRepo,
                           UserRepo userRepo) {
        this.discover = discover;
        this.groupRepo = groupRepo;
        this.userRepo = userRepo;
    }

    @GetMapping(value = "/api/groups")
    public List<GroupResponse> getAllGroups(){
        String userId = discover.getLoggedUserId();
        User user = discover.getLoggedUser();
        return groupRepo.findByOwner(userId)
                .stream()
                .map(group -> new GroupResponse(group, user))
                .collect(Collectors.toList());
    }

    @PostMapping(value = "/api/groups")
    public void createNewGroup(@RequestBody GroupRequest req) {
        String userId = discover.getLoggedUserId();
        List<User> users = getUsersFromIds(req.getUsers());
        Group group = Group.createNew(req.getName(), userId, users);
        groupRepo.save(group);
    }

    @GetMapping(value = "/api/groups/{id}")
    public GroupResponse getGroupDetails(@PathVariable String id){
        Group group = groupRepo.findOne(id);
        User owner = userRepo.findOne(group.getOwner());
        return new GroupResponse(group, owner);
    }

    @PostMapping(value = "/api/groups/{id}")
    public void addUsers(@PathVariable String id,
                         @RequestBody UserRequest req) {
        Group group = groupRepo.findOne(id);
        List<User> users = getUsersFromIds(req.getUserIds());
        group.addUsers(users);
        groupRepo.save(group);
    }

    @PutMapping(value = "/api/groups/{id}")
    public void updateGroup(@PathVariable String id,
                            @RequestBody GroupRequest req) {
        Group group = groupRepo.findOne(id);
        List<User> users = getUsersFromIds(req.getUsers());
        group.update(req.getName(), users);
        groupRepo.save(group);
    }

    @DeleteMapping(value = "/api/groups/{id}")
    public void deleteGroup(@PathVariable String id) {
        groupRepo.delete(id);
    }

    @DeleteMapping(value = "/api/groups/{groupId}/{userId}")
    public void deleteUserFromGroup(@PathVariable String groupId,
                                    @PathVariable String userId) {
        Group group = groupRepo.findOne(groupId);
        group.deleteUser(userId);
        groupRepo.save(group);
    }

    @GetMapping(value = "/api/groups/{id}/users")
    public List<UserGroupResponse> getUsersFromGroup(@PathVariable String id) {
        Group group = groupRepo.findOne(id);
        return group.getUsers()
                .stream()
                .map(UserGroupResponse::new)
                .collect(Collectors.toList());
    }

    private List<User> getUsersFromIds(List<String> users){
        return users.stream()
                .map(userRepo::findOne)
                .collect(Collectors.toList());
    }
}
