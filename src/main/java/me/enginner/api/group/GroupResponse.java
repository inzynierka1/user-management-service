package me.enginner.api.group;

import lombok.Data;
import me.enginner.domain.Group;
import me.enginner.domain.User;

import java.util.List;
import java.util.stream.Collectors;

@Data
public class GroupResponse {

    private String id;
    private String name;
    private String creation;
    private UserGroupResponse owner;
    private List<UserGroupResponse> users;

    public GroupResponse(Group group, User owner) {
        this.id = group.getStringId();
        this.name = group.getName();
        this.creation = group.getCreation().toString();
        this.owner = new UserGroupResponse(owner);
        this.users = group.getUsers()
                .stream()
                .map(UserGroupResponse::new)
                .collect(Collectors.toList());
    }
}
