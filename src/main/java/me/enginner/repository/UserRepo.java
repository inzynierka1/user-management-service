package me.enginner.repository;

import me.enginner.domain.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepo extends MongoRepository<User, String> {
    User findByEmail(String email);
    List<User> findTop10ByEmailContaining(String query);
}
