package me.enginner.repository;

import me.enginner.domain.Group;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.stream.Stream;

public interface GroupRepo extends MongoRepository<Group, String> {
    List<Group> findAllByUsersContains(String userId);
    List<Group> findByOwner(String owner);
    Group findFirstByOwnerAndNameContaining(String owner, String name);
}