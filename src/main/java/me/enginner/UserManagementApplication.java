package me.enginner;

import me.enginner.config.API;
import me.enginner.config.ConfigProvider;
import me.enginner.config.EnvVariablesConfigProvider;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("me.enginner")
@EnableAutoConfiguration
public class UserManagementApplication extends SpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserManagementApplication.class, args);
    }

    @Bean
    API initApi() {
        ConfigProvider configProvider = new EnvVariablesConfigProvider();
        return configProvider.getApiConfig();
    }
}
