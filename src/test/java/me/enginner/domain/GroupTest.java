package me.enginner.domain;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class GroupTest {

    private List<User> updatedUsers;
    private Group group;

    @Before
    public void setup() {
        List<User> usersInGroup = new ArrayList<>();
        updatedUsers = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            User mock = User.register("kamil" + i, "pass" + i);
            usersInGroup.add(mock);
            if (i < 2) {
                updatedUsers.add(mock);
            }
        }
        group = Group.createNew("name", "owner", usersInGroup);
    }

    @Test
    public void testEnv() {
        assertEquals(5, group.getUsers().size());
        assertEquals(2, updatedUsers.size());
    }

    @Test
    public void updateShouldChangeName() {
        String newName = "newName";
        group.update(newName, new ArrayList<>());
        assertEquals(group.getName(), newName);
    }

    @Test
    public void updateShouldDeleteAllUsersOnEmptyList() {
        group.update("apsik", new ArrayList<>());
        assertEquals(0, group.getUsers().size());
    }

    @Test
    public void updateShouldAddNewUsers() {
        updatedUsers.add(User.register("wojtek","haslo"));
        group.update("apsik", updatedUsers);
        assertEquals(3, group.getUsers().size());
    }

    @Test
    public void updateShouldKeepOldUsers() {
        group.update("apsik", updatedUsers);
        assertEquals(2, group.getUsers().size());
    }
}