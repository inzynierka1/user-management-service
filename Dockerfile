FROM java

ADD auth-management-service-0.0.1.jar /app
ENTRYPOINT [ "sh", "-c", "java -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5005 -jar /app" ]